FROM tomcat:7-jre8

ENV DOCKERIZE_VERSION v0.2.0

RUN curl -L "https://github.com/jwilder/dockerize/releases/download/${DOCKERIZE_VERSION}/dockerize-linux-amd64-${DOCKERIZE_VERSION}.tar.gz" -o "/tmp/dockerize-linux-amd64-${DOCKERIZE_VERSION}.tar.gz" \
    && tar -C /usr/local/bin -xzvf "/tmp/dockerize-linux-amd64-${DOCKERIZE_VERSION}.tar.gz"

RUN cd /usr/local/tomcat/webapps \
&& curl -L "https://dl.bintray.com/librehealth/lh-toolkit-war/lh-toolkit.war" -o lh-toolkit.war


ADD lh-toolkit-runtime.properties "/usr/local/tomcat/lh-toolkit-runtime.properties"

CMD ["dockerize","-wait","tcp://db:3306","-timeout","30s","catalina.sh","run"]